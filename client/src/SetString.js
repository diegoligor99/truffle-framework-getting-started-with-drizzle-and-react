import React from "react";

class SetString extends React.Component {
	state = { stackId: null };

	handleKeyDown = e => {
		// if the enter key is pressed, set the value with the string
		if (e.keyCode === 13) {
			this.setValue(e.target.value);
		}
	};

	setValue = value => {
		const { drizzle, drizzleState } = this.props;
		const contract = drizzle.contracts.MyStringStore;

		// let drizzle know we want to call the `set` method with `value`
		/**
		 *  We first assign the contract from the drizzle instance into contract, and then we call cacheSend() on the method we are interested in (i.e. set). Then we pass in the string we want to set (i.e. value) as well as our transaction options (in this case, just the from field). Note that we can get our current account address from drizzleState.accounts[0].
		 *  What we get in return is a stackId, which is a reference to the transaction that we want to execute. Ethereum transactions don't receive a hash until they're broadcast to the network. In case an error occurs before broadcast, Drizzle keeps track of these transactions by giving each it's own ID. Once successfully broadcasted, the stackId will point to the transaction hash, so we save it in our local component state for later usage.
		 */
		const stackId = contract.methods["set"].cacheSend(value, {
			from: drizzleState.accounts[0]
		});

		// save the `stackId` for later reference
		this.setState({ stackId });
	};

	getTxStatus = () => {
		// get the transaction states from the drizzle state
		const { transactions, transactionStack } = this.props.drizzleState;

		// get the transaction hash using our saved `stackId`
		/** Now that we have a stackId saved into our local component state, we can use this to check the status of our transaction. First, we need the transactions and transactionStack slices of state from drizzleState. */
		const txHash = transactionStack[this.state.stackId];

		// if transaction hash does not exist, don't display anything
		/** Then, we can get the transaction hash (assigned to txHash) via transactionStack[stackId]. If the hash does not exist, then we know that the transaction has not been broadcasted yet and we return null. */
		if (!txHash) return null;

		// otherwise, return the transaction status
		return `Transaction status: ${transactions[txHash].status}`;
	};

	render() {
		return (
			<div>
				<input type="text" onKeyDown={this.handleKeyDown} />
				<div>{this.getTxStatus()}</div>
			</div>
		);
	}
}

export default SetString;
